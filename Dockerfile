# syntax=docker/dockerfile:experimental
ARG PHP_VERSION
FROM registry.gitlab.com/jitesoft/dockerfiles/php/cli:${PHP_VERSION}
ARG COMPOSER_VERSION
LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>" \
      maintainer.org="Jitesoft" \
      maintainer.org.uri="https://jitesoft.com" \
      com.jitesoft.project.repo.type="git" \
      com.jitesoft.project.repo.uri="https://gitlab.com/jitesoft/dockerfiles/composer-alpine" \
      com.jitesoft.project.repo.issues="https://gitlab.com/jitesoft/dockerfiles/composer-alpine/issues" \
      com.jitesoft.project.registry.uri="registry.gitlab.com/jitesoft/dockerfiles/composer-alpine" \
      com.jitesoft.app.composer.version="${COMPOSER_VERSION}" \
      # Open container labels
      org.opencontainers.image.version="${PHP_VERSION}" \
      org.opencontainers.image.created="${BUILD_TIME}" \
      org.opencontainers.image.description="PHP and Composer on alpine linux" \
      org.opencontainers.image.vendor="Jitesoft" \
      org.opencontainers.image.source="https://gitlab.com/jitesoft/dockerfiles/composer-alpine" \
      # Artifact hub annotations
      io.artifacthub.package.alternative-locations="oci://registry.gitlab.com/jitesoft/dockerfiles/composer-alpine,oci://index.docker.io/jitesoft/composer,oci://ghcr.io/jitesoft/composer,oci://quay.io/jitesoft/composer" \
      io.artifacthub.package.readme-url="https://gitlab.com/jitesoft/dockerfiles/composer-alpine/-/raw/master/README.md" \
      io.artifacthub.package.logo-url="https://jitesoft.com/favicon-96x96.png"


ENV COMPOSER_ALLOW_SUPERUSER="1" \
    COMPOSER_HOME="/composer" \
    PATH="/composer/vendor/bin:$PATH" \
    COMPOSER_NO_INTERACTION="1"

ARG COMPOSER_VERSION

RUN --mount=type=bind,source=./downloads,target=/tmp/bin \
    echo "memory_limit=-1" > $PHP_INI_DIR/conf.d/memory-limit.ini \
 && echo "date.timezone=${PHP_TIMEZONE:-UTC}" > $PHP_INI_DIR/conf.d/date_timezone.ini \
 && php /tmp/bin/composer-setup.php --install-dir=/usr/local/bin --filename=composer \
 && composer -V \
 && php --version

ENTRYPOINT ["entrypoint"]
CMD ["composer"]
